<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class ExamIdentity extends CUserIdentity
{
 private $_id;

	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
		/*$users=array(
			// username => password
			'demo'=>'demo',
			'admin'=>'admin',
		);*/
                
            
        // print_r($this); die(); 
			
		$user=User::model()->findByAttributes(array('username'=>$this->username));
		if(!isset($user))
			$this->errorCode=self::ERROR_USERNAME_INVALID;
		else if($user->password!==($this->password))
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
		else
		{
			$this->errorCode=self::ERROR_NONE;
			$this->_id=$user->id;
			$this->setState("group_id",$user->group_id);
			$this->setState("password",$user->password);
			$this->setState("username",$user->username);
			$this->setState("tsp_id",$user->tsp_id);
		
		
		}
		return !$this->errorCode;
	}
	public function getID()
	{
	return $this->_id;
	}
	

	
}