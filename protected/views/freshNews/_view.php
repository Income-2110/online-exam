<?php
/* @var $this FreshNewsController */
/* @var $data FreshNews */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('title')); ?>:</b>
	<?php echo CHtml::encode($data->title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dec')); ?>:</b>
	<?php echo CHtml::encode($data->dec); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('creat_date')); ?>:</b>
	<?php echo CHtml::encode($data->creat_date); ?>
	<br />


</div>