<?php
/* @var $this FreshNewsController */
/* @var $model FreshNews */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'fresh-news-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dec'); ?>
		<?php echo $form->textArea($model,'dec',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'dec'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'creat_date'); ?>
		<?php
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,
                'name' => 'FreshNews[creat_date]',
                'value'=> $model->creat_date,
                'options' => array(
                    'dateFormat' => 'yy-mm-dd',
                    //'altFormat' => 'yy-mm-dd', // show to user format
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'focus',
                    'buttonImageOnly' => false,
                ),
                'htmlOptions'=>array(
                    'class'=>'span5',
                ),
            ));
            ?>
		<?php echo $form->error($model,'creat_date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->