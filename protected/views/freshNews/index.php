<?php
/* @var $this FreshNewsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Fresh News',
);

$this->menu=array(
	array('label'=>'Create FreshNews', 'url'=>array('create')),
	array('label'=>'Manage FreshNews', 'url'=>array('admin')),
);
?>

<h1>Fresh News</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
