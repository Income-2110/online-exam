<?php
/* @var $this FreshNewsController */
/* @var $model FreshNews */

$this->breadcrumbs=array(
	'Fresh News'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List FreshNews', 'url'=>array('index')),
	array('label'=>'Create FreshNews', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#fresh-news-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Fresh News</h1>

 

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'fresh-news-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'title',
		'dec',
		'creat_date',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
