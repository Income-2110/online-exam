<?php
/* @var $this QuestionController */
/* @var $model Question */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'question-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'questions'); ?>
		<?php echo $form->textArea($model,'questions',array('rows'=>3, 'cols'=>50)); ?>
		<?php echo $form->error($model,'questions'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'correct_answer'); ?>
		<?php echo $form->textArea($model,'correct_answer',array('rows'=>3, 'cols'=>50)); ?>
		<?php echo $form->error($model,'correct_answer'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($option,'option'); ?>
		<?php echo $form->textArea($option,'option[]',array('rows'=>3, 'cols'=>50)); ?>
		<?php echo $form->error($option,'option'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($option,'option'); ?>
		<?php echo $form->textArea($option,'option[]',array('rows'=>3, 'cols'=>50)); ?>
		<?php echo $form->error($option,'option'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($option,'option'); ?>
		<?php echo $form->textArea($option,'option[]',array('rows'=>3, 'cols'=>50)); ?>
		<?php echo $form->error($option,'option'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($option,'option'); ?>
		<?php echo $form->textArea($option,'option[]',array('rows'=>3, 'cols'=>50)); ?>
		<?php echo $form->error($option,'option'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'batch_id'); ?>
		<?php echo $form->dropDownList($model,'batch_id', CHtml::listData(
	     batch::model()->findAll(array('order'=>'id')),'id','batch_name')); ?>
		<?php echo $form->error($model,'batch_id'); ?>
	</div>

	<?php 
	$id=yii::app()->user->Id;
	$user = Trainer::model()->findAll("id=$id");
	foreach($user as $data):
		$tsp_id=$data->tsp_id;
	?>
	
	<div class="row">
		<?php echo $form->labelEx($model,'tsp'); ?>
		<?php echo $form->dropDownList($model,'tsp_id', CHtml::listData(
	     traningCenter::model()->findAll(array('order'=>'id','condition'=>"id=$tsp_id")),'id','tsp_name')); ?>
	</div>
	
	<?php endforeach ?>
	
	<div class="row">
		<?php echo $form->labelEx($model,'class_id'); ?>
		<?php echo $form->dropDownList($model,'class_id', CHtml::listData(
	     classTest::model()->findAll(array('order'=>'id')),'id','class_name')); ?>
		<?php echo $form->error($model,'class_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->