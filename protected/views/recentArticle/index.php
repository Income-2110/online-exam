<?php
/* @var $this RecentArticleController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Recent Articles',
);

$this->menu=array(
	array('label'=>'Create RecentArticle', 'url'=>array('create')),
	array('label'=>'Manage RecentArticle', 'url'=>array('admin')),
);
?>

<h1>Recent Articles</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
