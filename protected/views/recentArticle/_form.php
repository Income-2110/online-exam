<?php
/* @var $this RecentArticleController */
/* @var $model RecentArticle */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'recent-article-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'totle'); ?>
		<?php echo $form->textField($model,'totle',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'totle'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dec'); ?>
		<?php echo $form->textArea($model,'dec',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'dec'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->