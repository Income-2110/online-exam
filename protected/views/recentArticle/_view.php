<?php
/* @var $this RecentArticleController */
/* @var $data RecentArticle */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('totle')); ?>:</b>
	<?php echo CHtml::encode($data->totle); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dec')); ?>:</b>
	<?php echo CHtml::encode($data->dec); ?>
	<br />


</div>