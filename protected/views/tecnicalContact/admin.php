<?php
/* @var $this TecnicalContactController */
/* @var $model TecnicalContact */

$this->breadcrumbs=array(
	'Tecnical Contacts'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List TecnicalContact', 'url'=>array('index')),
	array('label'=>'Create TecnicalContact', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#tecnical-contact-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Tecnical Contacts</h1>

 

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'tecnical-contact-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'group_id',
		'tsp_id',
		'name',
		'username',
		'password',
		'address',
		'phone',
		'email', 
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
