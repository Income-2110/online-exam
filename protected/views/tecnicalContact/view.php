<?php
/* @var $this TecnicalContactController */
/* @var $model TecnicalContact */

$this->breadcrumbs=array(
	'Tecnical Contacts'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List TecnicalContact', 'url'=>array('index')),
	array('label'=>'Create TecnicalContact', 'url'=>array('create')),
	array('label'=>'Update TecnicalContact', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete TecnicalContact', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TecnicalContact', 'url'=>array('admin')),
);
?>

<h1>View TecnicalContact #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'group_id',
		'tsp_id',
		'name',
		'username',
		'password',
		'address',
		'phone',
		'email',
		
	),
)); ?>
