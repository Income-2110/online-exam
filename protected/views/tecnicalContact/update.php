<?php
/* @var $this TecnicalContactController */
/* @var $model TecnicalContact */

$this->breadcrumbs=array(
	'Tecnical Contacts'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);
?>

<h1>Update TecnicalContact <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>