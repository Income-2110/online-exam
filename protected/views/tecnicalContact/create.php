<?php
/* @var $this TecnicalContactController */
/* @var $model TecnicalContact */

$this->breadcrumbs=array(
	'Tecnical Contacts'=>array('index'),
	'Create',
);
?>

<h1>Create TecnicalContact</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>