<?php
/* @var $this TrainerController */
/* @var $model Trainer */

$this->breadcrumbs=array(
	'Trainers'=>array('index'),
	$model->name,
);
?>

<h1>View Trainer #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		array(
           'name'=>'group_id', 
           'value'=>CHtml::encode($model->group_rel->group_name), 
		    ),
		array(
           'name'=>'tsp_id', 
           'value'=>CHtml::encode($model->tsp_rel->tsp_name), 
		    ),
		'name',
		'username',
		'password',
		'address',
		'phone',
		'picture',
		
	),
)); ?>
