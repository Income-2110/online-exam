<?php
/* @var $this ClassTestController */
/* @var $model ClassTest */

$this->breadcrumbs=array(
	'Class Tests'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List ClassTest', 'url'=>array('index')),
	array('label'=>'Create ClassTest', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#class-test-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Class Tests</h1>



<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'class-test-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'class_name',
		'module_id',
		'course_id',
		'creat_date',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
