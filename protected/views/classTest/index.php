<?php
/* @var $this ClassTestController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Class Tests',
);

$this->menu=array(
	array('label'=>'Create ClassTest', 'url'=>array('create')),
	array('label'=>'Manage ClassTest', 'url'=>array('admin')),
);
?>

<h1>Class Tests</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
