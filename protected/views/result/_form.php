<?php
/* @var $this ResultController */
/* @var $model Result */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'result-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'username'); ?>
		<?php echo $form->textField($model,'username',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'batch_id'); ?>
		<?php echo $form->dropDownList($model,'batch_id', CHtml::listData(
	     batch::model()->findAll(array('order'=>'id')),'id','batch_name')); ?>
		<?php echo $form->error($model,'batch_id'); ?>
	</div>

	<?php 
	$id=yii::app()->user->Id;
	$user = Trainer::model()->findAll("id=$id");
	foreach($user as $data):
		$tsp_id=$data->tsp_id;
	?>
	
	<div class="row">
		<?php echo $form->labelEx($model,'tsp'); ?>
		<?php echo $form->dropDownList($model,'tsp_id', CHtml::listData(
	     traningCenter::model()->findAll(array('order'=>'id','condition'=>"id=$tsp_id")),'id','tsp_name')); ?>
	</div>
	
	<?php endforeach ?>

	<div class="row">
		<?php echo $form->labelEx($model,'course_id'); ?>
		<?php echo $form->dropDownList($model,'course_id', CHtml::listData(
	     Course::model()->findAll(array('order'=>'id')),'id','course_name')); ?>
		<?php echo $form->error($model,'course_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'module_id'); ?>
		<?php echo $form->dropDownList($model,'module_id', CHtml::listData(
	     Module::model()->findAll(array('order'=>'id')),'id','module_name')); ?>
		<?php echo $form->error($model,'module_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'class_id'); ?>
		<?php echo $form->dropDownList($model,'class_id', CHtml::listData(
	     classTest::model()->findAll(array('order'=>'id')),'id','class_name')); ?>
		<?php echo $form->error($model,'class_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'marks'); ?>
		<?php echo $form->textField($model,'marks',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'marks'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'exam_marks'); ?>
		<?php echo $form->textField($model,'exam_marks',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'exam_marks'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'update_date'); ?>
		<?php
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,
                'name' => 'Result[update_date]',
                'value'=> $model->update_date,
                'options' => array(
                    'dateFormat' => 'yy-mm-dd',
                    //'altFormat' => 'yy-mm-dd', // show to user format
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'focus',
                    'buttonImageOnly' => false,
                ),
                'htmlOptions'=>array(
                    'class'=>'span5',
                ),
            ));
            ?>
		<?php echo $form->error($model,'update_date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->