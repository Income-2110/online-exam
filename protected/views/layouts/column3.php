<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/my_layout_admin'); ?>



<div class="span-5 last" style="float:right; padding-right:30px; padding-top:20px">
	<div id="sidebar">
	<?php
		$this->beginWidget('zii.widgets.CPortlet', array(
			'title'=>'Operations',
		));
		$this->widget('zii.widgets.CMenu', array(
			'items'=>$this->menu,
			'htmlOptions'=>array('class'=>'operations'),
		));
		$this->endWidget();
	?>
	</div><!-- sidebar -->
</div>
<div style="width:800px;padding-left:100px;">
		<?php echo $content; ?>
</div>


<?php $this->endContent(); ?>