<?php

class SiteController extends Controller
{
	public $layout='//layouts/my_layout';
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->render('index');
	}
	
		public function actionAdminPage()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->layout="my_layout_admin2";
		$this->render('adminPage');
	}
	
	public function actionTraningCenter()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->render('traningCenter');
	}
	
	public function actionQuestion()
	{
		// $all_id;
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
	
		$this->layout="my_layout_trainee";
		  if(isset($_POST['Question']) )
		{
		   $all_id=array();
		   $all_id=$_REQUEST['Question'];
			
		}
		if(isset($_REQUEST['Question']))
		 $this->render('question', array('all_id'=>$all_id));
		else
         $this->render('refreshMessage');
	}
	
	public function actionSolution()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		
		$this->layout="my_layout_trainee";
		if(isset($_POST['Options']) )
		{
		   $question_id=array();
		   $question_id=$_REQUEST['Options']["option"];
			
		}
		if(isset($_REQUEST['Options']["option"]))
			$this->render('solution', array('question_id'=>$question_id));
		else
			$this->render('unselectSubmit');
	}
	
	public function actionTrainerHomePage()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->layout="my_layout_trainer2";
		$this->render('trainerHomePage');
	}
	
	public function actionSetQuestionPaper()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->render('setQuestionPaper');
	}
	
	public function actionTecnicalContactHomePage()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->layout="my_layout_tsp2";
		$this->render('tecnicalContactHomePage');
	}
	
	public function actionSelectCatagory()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->layout="my_layout_trainee";
		$this->render('selectCatagory');
	}
	
	public function actionRegistration()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->render('registration');
	}


	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect('index.php?r=site/adminPage');
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}
	
		public function actionLoginTrainer()
	{
		$model=new LoginTrainer;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginTrainer']))
		{
			$model->attributes=$_POST['LoginTrainer'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect('index.php?r=site/trainerHomePage');
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}
	
			public function actionLoginTsp()
	{
		$model=new LoginTsp;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginTsp']))
		{
			$model->attributes=$_POST['LoginTsp'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect('index.php?r=site/tecnicalContactHomePage');
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}
	
	public function actionExam()
	{
		$model=new LoginExam;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='user-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginExam']))
		{
			$model->attributes=$_POST['LoginExam'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect('index.php?r=site/selectCatagory');
		}
		// display the login form
		$this->render('exam',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}