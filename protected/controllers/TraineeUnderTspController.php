<?php

class TraineeUnderTspController extends Controller
{

	public $layout='//layouts/column3';
	
	public function actionIndex()
	{
		$model=new TraningCenter;
		$this->render('index',array(
			'model'=>$model,
		));
	}

	// Uncomment the following methods and override them if needed
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			/* array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			), */
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('index','view','create','update','admin','delete','traineeUnderTsp'),
				'expression'=>"isset(Yii::app()->user->group_id)&& Yii::app()->user->group_id==1",
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	public function actionTraineeUnderTsp()
	{
	 $model=new TraningCenter;
	 
		if(isset($_POST['TraningCenter']))
		{
		   $id=$_POST['TraningCenter']["id"];
 	 
		}
		if(isset($_POST['TraningCenter']))   
			$this->render('traineeUnderTsp', array('id'=>$id));
		else
			$this->render('index');
	}
}