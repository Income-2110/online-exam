<?php

/**
 * This is the model class for table "course_under_tsp".
 *
 * The followings are the available columns in table 'course_under_tsp':
 * @property integer $id
 * @property integer $tsp_id
 * @property integer $course_id
 * @property integer $batch_id
 * @property integer $trainer_id
 */
class CourseUnderTsp extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CourseUnderTsp the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'course_under_tsp';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tsp_id, course_id, batch_id, trainer_id', 'required'),
			array('tsp_id, course_id, batch_id, trainer_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, tsp_id, course_id, batch_id, trainer_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		'batch_rel'=>array(self::BELONGS_TO,'Batch','batch_id'),
		'tsp_rel'=>array(self::BELONGS_TO,'TraningCenter','tsp_id'),
		'course_rel'=>array(self::BELONGS_TO,'Course','course_id'),
		'trainer_rel'=>array(self::BELONGS_TO,'Trainer','trainer_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'tsp_id' => 'Tsp',
			'course_id' => 'Course',
			'batch_id' => 'Batch',
			'trainer_id' => 'Trainer',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->with = array('batch_rel');
		$criteria->compare('batch_rel.batch_name', $this->batch_id, true );
		$criteria->with = array('tsp_rel');
		$criteria->compare('tsp_rel.tsp_name', $this->tsp_id, true );
		$criteria->with = array('course_rel');
		$criteria->compare('course_rel.course_name', $this->course_id, true );
		$criteria->with = array('trainer_rel');
		$criteria->compare('trainer_rel.name', $this->trainer_id, true );

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}